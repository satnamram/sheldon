		<?php
		/**
		 *  The template for displaying Footer.
		 *
		 *  @package sheldon
		 */
		?>
		<?php
		// customizr overwrite
				echo '<style>';
				echo '#footer{';
				echo '	background-color: '.get_theme_mod( 'sheldon_footer_bgcolor','#c3d8a2').';';
				echo '	color: '.get_theme_mod( 'sheldon_footer_fgcolor','#000000').';';
				echo '}';
				echo '</style>';
		?>
		<footer id="footer">
			<div class="wrapper cf">
					<?php
						if ( get_theme_mod( 'sheldon_footer_partner',get_template_directory_uri().'/images/partner.png' ) ) {
							echo '<div id="partner-box">';
							  echo '<p> Partner im: </p><br>';
								echo '<img style="width:auto;height:100%;" src="'.get_theme_mod( 'sheldon_footer_partner', get_template_directory_uri().'/images/partner.png' ).'" alt="Partner"/>';
							echo '</div>';
						}
						if ( get_theme_mod( 'sheldon_footer_copyright','©® PEKiP und das PEKiP-Logo sind eingetragene Zeichen des PEKiP e.V' ) ) {
							echo '<div id="copyright-box">';
								echo get_theme_mod( 'sheldon_footer_copyright','©® PEKiP und das PEKiP-Logo sind eingetragene Zeichen des PEKiP e.V' );
							echo '</div>';
						}
					?>
			</div><!--/div .wrapper .cf-->
		</footer><!--/footer #footer-->
		<?php wp_footer(); ?>
	</body>
</html>
