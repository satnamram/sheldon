<?php

function sheldon_customizer( $wp_customize ) {

	class sheldon_Theme_Support extends WP_Customize_Control
	{
		public function render_content()
		{

		}

	}


    $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
    $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
    $wp_customize->get_setting( 'background_color' )->transport = 'postMessage';

    /*
    ** Logo Customizer
    */
    $wp_customize->add_section( 'sheldon_header_section' , array(
    	'title'       => __( 'Kopfzeile', 'sheldon' ),
    	'priority'    => 40,
   	) );

		/* Header - Logo */
		$wp_customize->add_setting( 'sheldon_header_logo',
        array('sanitize_callback' => 'esc_url_raw', 'default' => get_template_directory_uri() .'/images/header-logo.png') );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sheldon_header_logo', array(
		    'label'    => __( 'Logo:', 'sheldon' ),
		    'section'  => 'sheldon_header_section',
		    'settings' => 'sheldon_header_logo',
		    'priority' => '1',
		) ) );

		$wp_customize->add_setting( 'sheldon_header_mobile_fgcolor',
			array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#111111','sheldon')));
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_header_mobile_fgcolor',array(
		'label'      => __( 'Mobile Menu Schrift', 'sheldon' ),
		'section'    => 'sheldon_header_section',
		'settings'   => 'sheldon_header_mobile_fgcolor',
	) ));

			$wp_customize->add_setting( 'sheldon_header_mobile_bgcolor',
				array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#9fda40','sheldon')));
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_header_mobile_bgcolor',array(
			'label'      => __( 'Mobile Menu Hintergrund', 'sheldon' ),
			'section'    => 'sheldon_header_section',
			'settings'   => 'sheldon_header_mobile_bgcolor',
		) ));

	$wp_customize->add_setting( 'sheldon_header_menu_color',
		array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#FFFFFF','sheldon')));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_header_menu_color',array(
	'label'      => __( 'Menu Farbe', 'sheldon' ),
	'section'    => 'sheldon_header_section',
	'settings'   => 'sheldon_header_menu_color',
) ));

		/*
    ** Contact Customizer
    */
    $wp_customize->add_section( 'sheldon_contact_section' , array(
    	'title'       => __( 'Kontakt', 'sheldon' ),
    	'priority'    => 41,
   	) );

		/*
    ** Page Customizer
    */
    $wp_customize->add_section( 'sheldon_page_section' , array(
    	'title'       => __( 'Seite', 'sheldon' ),
    	'priority'    => 42,
   	) );

		$wp_customize->add_setting( 'sheldon_page_h_fgcolor',
			array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#dc2b19','sheldon')));
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_page_h_fgcolor',array(
		'label'      => __( 'Farbe Überschrift 1', 'sheldon' ),
		'section'    => 'sheldon_page_section',
		'settings'   => 'sheldon_page_h_fgcolor',
	) ));


		$wp_customize->add_setting( 'sheldon_page_h_bgcolor',
			array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#ec9746','sheldon')));
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_page_h_bgcolor',array(
		'label'      => __( 'Hintergrundfarbe Überschrift 1', 'sheldon' ),
		'section'    => 'sheldon_page_section',
		'settings'   => 'sheldon_page_h_bgcolor',
	) ));

	$wp_customize->add_setting( 'sheldon_page2_h_fgcolor',
		array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#dc2b19','sheldon')));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_page2_h_fgcolor',array(
	'label'      => __( 'Farbe Überschrift 2', 'sheldon' ),
	'section'    => 'sheldon_page_section',
	'settings'   => 'sheldon_page2_h_fgcolor',
) ));


	$wp_customize->add_setting( 'sheldon_page2_h_bgcolor',
		array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#c3d8a2','sheldon')));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_page2_h_bgcolor',array(
	'label'      => __( 'Hintergrundfarbe Überschrift 2', 'sheldon' ),
	'section'    => 'sheldon_page_section',
	'settings'   => 'sheldon_page2_h_bgcolor',
) ));


		/*
		** Footer Customizer
		*/
		$wp_customize->add_section( 'sheldon_footer_section' , array(
			'title'       => __( 'Fußzeile', 'sheldon' ),
			'priority'    => 250,
    ) );

   	/* Footer - Partner Box */
	  $wp_customize->add_setting( 'sheldon_footer_partner' ,
			array('default' => get_template_directory_uri().'/images/partner.png','sanitize_callback' => 'esc_url_raw'));
	  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sheldon_footer_partner', array(
			'label'    => __( 'Partner Bild:', 'sheldon' ),
			'section'  => 'sheldon_footer_section',
			'settings' => 'sheldon_footer_partner',
			'priority' => '7',
	  ) ) );

		/* Footer - Copyright Box */
		$wp_customize->add_setting( 'sheldon_footer_copyright' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => '©® PEKiP und das PEKiP-Logo sind eingetragene Zeichen des PEKiP e.V'));
		$wp_customize->add_control( 'sheldon_header_subtitle', array(
		    'label'    => __( 'Copyright Eintrag', 'sheldon' ),
		    'section'  => 'sheldon_footer_section',
		    'settings' => 'sheldon_footer_copyright',
			'priority' => '3',
		) );

		$wp_customize->add_setting( 'sheldon_footer_fgcolor',
			array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#111111','sheldon')));
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_footer_fgcolor',array(
		'label'      => __( 'Farbe', 'sheldon' ),
		'section'    => 'sheldon_footer_section',
		'settings'   => 'sheldon_footer_fgcolor',
	) ));


		$wp_customize->add_setting( 'sheldon_footer_bgcolor',
			array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __('#c3d8a2','sheldon')));
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sheldon_footer_bgcolor',array(
		'label'      => __( 'Hintergrundfarbe', 'sheldon' ),
		'section'    => 'sheldon_footer_section',
		'settings'   => 'sheldon_footer_bgcolor',
	) ));


    /*
    ** Front Page Customizer
    */
    $wp_customize->add_section( 'sheldon_frontpage_section' , array(
    	'title'       => __( 'Startseite', 'sheldon' ),
    	'priority'    => 42,
	) );

	/* Front Page - Subheader Background */
	$wp_customize->add_setting( 'sheldon_frontpage_subheader_bg', array('default' => get_template_directory_uri() . "/images/full-header.jpg", 'sanitize_callback' => 'esc_url_raw') );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sheldon_frontpage_subheader_bg', array(
			'label'    => __( 'Banner:', 'sheldon' ),
			'section'  => 'sheldon_frontpage_section',
			'settings' => 'sheldon_frontpage_subheader_bg',
			'priority' => '4',
	) ) );


		/* Front Page - Firstly Box - Title */
		$wp_customize->add_setting( 'sheldon_frontpage_firstlybox_title' ,
        array('sanitize_callback' => 'sheldon_sanitize_text','default' => __('Lorem','sheldon')));
		$wp_customize->add_control( 'sheldon_frontpage_firstlybox_title', array(
		    'label'    => __( '#1 Titel:', 'sheldon' ),
		    'section'  => 'sheldon_frontpage_section',
		    'settings' => 'sheldon_frontpage_firstlybox_title',
			'priority' => '8',
		) );

		/* Front Page - Firstly Box - Content */
		$wp_customize->add_setting( 'sheldon_frontpage_firstlybox_content' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' =>  __( 'Go to Appearance - Customize, to add content.', 'sheldon' )));
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'sheldon_frontpage_firstlybox_content', array(
		            'label' 	=> __( '#1 Text:', 'sheldon' ),
		            'section' 	=> 'sheldon_frontpage_section',
		            'settings' 	=> 'sheldon_frontpage_firstlybox_content',
		            'priority' 	=> '9'
		        )
		    )
		);

		/* Front Page - Secondly Box - Title */
		$wp_customize->add_setting( 'sheldon_frontpage_secondlybox_title' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __( 'Ipsum', 'sheldon' )));
		$wp_customize->add_control( 'sheldon_frontpage_secondlybox_title', array(
		    'label'    => __( '#2 Titel:', 'sheldon' ),
		    'section'  => 'sheldon_frontpage_section',
		    'settings' => 'sheldon_frontpage_secondlybox_title',
			'priority' => '11',
		) );

		/* Front Page - Secondly Box - Content */
		$wp_customize->add_setting( 'sheldon_frontpage_secondlybox_content' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __( 'Go to Appearance - Customize, to add content.', 'sheldon' )));
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'sheldon_frontpage_secondlybox_content', array(
		            'label' 	=> __( '#2 Text:', 'sheldon' ),
		            'section' 	=> 'sheldon_frontpage_section',
		            'settings' 	=> 'sheldon_frontpage_secondlybox_content',
		            'priority' 	=> '12'
		        )
		    )
		);

		/* Front Page - Thirdly Box - Title */
		$wp_customize->add_setting( 'sheldon_frontpage_thirdlybox_title' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __( 'Dolor', 'sheldon' )));
		$wp_customize->add_control( 'sheldon_frontpage_thirdlybox_title', array(
		    'label'    => __( '#3 Titel:', 'sheldon' ),
		    'section'  => 'sheldon_frontpage_section',
		    'settings' => 'sheldon_frontpage_thirdlybox_title',
			'priority' => '14',
		) );

		/* Front Page - Thirdly Box - Content */
		$wp_customize->add_setting( 'sheldon_frontpage_thirdlybox_content' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __( 'Go to Appearance - Customize, to add content.', 'sheldon' )));
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'sheldon_frontpage_thirdlybox_content', array(
		            'label' 	=> __( '#3 Text:', 'sheldon' ),
		            'section' 	=> 'sheldon_frontpage_section',
		            'settings' 	=> 'sheldon_frontpage_thirdlybox_content',
		            'priority' 	=> '15'
		        )
		    )
		);

		/* Front Page - Recent Post count */
		$wp_customize->add_setting( 'sheldon_frontpage_posts' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __( '3', 'sheldon' )));
		$wp_customize->add_control( 'sheldon_frontpage_posts', array(
		    'label'    => __( 'Anzahl der Blogeinträge:', 'sheldon' ),
		    'section'  => 'sheldon_frontpage_section',
		    'settings' => 'sheldon_frontpage_posts',
			'priority' => '17',
		) );



    /*
    ** 404 Customizer
    */
    $wp_customize->add_section( 'sheldon_404_section' , array(
    	'title'       => __( '404 Seite', 'sheldon' ),
    	'priority'    => 450,
	) );

		/* 404 - Title */
		$wp_customize->add_setting( 'sheldon_404_title' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __( '404 Error', 'sheldon' )));
		$wp_customize->add_control( 'sheldon_404_title', array(
		    'label'    => __( '404 - Title:', 'sheldon' ),
		    'section'  => 'sheldon_404_section',
		    'settings' => 'sheldon_404_title',
			'priority' => '1',
		) );

		/* 404 - Content */
		$wp_customize->add_setting( 'sheldon_404_content' ,
        array('sanitize_callback' => 'sheldon_sanitize_text', 'default' => __( 'Oops, I screwed up and you discovered my fatal flaw. Well, we\'re not all perfect, but we try.  Can you try this again or maybe visit our <a title="themeIsle" href="'. home_url() .'">Home Page</a> to start fresh.  We\'ll do better next time.', 'sheldon' )));
		$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'sheldon_404_content', array(
		            'label' 	=> __( '404 - Content', 'sheldon' ),
		            'section' 	=> 'sheldon_404_section',
		            'settings' 	=> 'sheldon_404_content',
		            'priority' 	=> '2'
		        )
		    )
		);

		function sheldon_sanitize_text( $input ) {
			return wp_kses_post( force_balance_tags( $input ) );
		}

		function sheldon_sanitize_none( $input ) {
			return $input;
		}
		function laweria_lite_sanitize_shortcode() {
			return force_balance_tags( $input );
		}

}
add_action( 'customize_register', 'sheldon_customizer' );

/**

 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.

 */

function sheldon_customize_preview_js() {

	wp_enqueue_script( 'sheldon_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );

}

add_action( 'customize_preview_init', 'sheldon_customize_preview_js' );

if( class_exists( 'WP_Customize_Control' ) ):
	class Example_Customize_Textarea_Control extends WP_Customize_Control {
	    public $type = 'textarea';

	    public function render_content() { ?>

	        <label>
	        	<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
	        	<textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
	        </label>

	        <?php
	    }
	}
endif;

?>
