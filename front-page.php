		<?php
		/**
		 *  The template for displaying Front Page.
		 *
		 *  @package sheldon
		 */
		get_header();
		if ( get_option( 'show_on_front' ) == 'page' ){?>
  			<section class="wide-nav">
				<div class="wrapper">
					<h3>
						<?php the_title(); ?>
					</h3><!--/h3-->
				</div><!--/div .wrapper-->
			</section><!--/section .wide-nav-->
		</header><!--/header-->
		<section id="content">
			<div class="wrapper cf">
				<div id="posts">
					<?php
						if ( have_posts() ) : while ( have_posts() ) : the_post();

					?>
					<div class="post">

						<div class="post-excerpt">
							<?php the_content(); ?>
						</div><!--/div .post-excerpt-->


					</div><!--/div .post-->
					<?php endwhile; else: ?>
                    	<p><?php _e('Sorry, no posts matched your criteria.', 'sheldon'); ?></p>
                	<?php endif; ?>
				</div><!--/div #posts-->
				<?php get_sidebar(); ?>
			</div><!--/div .wrapper-->
		</section><!--/section #content-->
<?php } else { ?>
			<div id="subheader" style="background-image: url('<?php
				if ( get_theme_mod( 'sheldon_frontpage_subheader_bg', get_template_directory_uri() . "/images/full-header.jpg" ) ) {
				    echo get_theme_mod( 'sheldon_frontpage_subheader_bg',get_template_directory_uri() . "/images/full-header.jpg" );
			     }
			 ?>');">
				<div class="subheader-color cf">
					<div class="wrapper cf">
						<div class="full-header-content">
						</div><!--/div .header-content-->
					</div><!--/div .wrapper-->
				</div><!--/div .full-header-color-->
<!--				<div class="second-subheader">
					<div class="wrapper">
						<h3 style="font-size: <?php if ( get_theme_mod( 'sheldon_frontpage_subheader_size' ) ) { echo get_theme_mod( 'sheldon_frontpage_subheader_size', '24px' ); } else { echo '24px'; } ?>;">
							<?php
								if ( get_theme_mod( 'sheldon_frontpage_subheader_title','Lorem Ipsum is simply dummy text of the printing and type setting industry.' ) ) {
									echo get_theme_mod( 'sheldon_frontpage_subheader_title','Lorem Ipsum is simply dummy text of the printing and type setting industry.' );
								}
							?>
						</h3><!--/h3--
					</div><!--/div .wrapper--
				</div><!--/div .second-subheader-->

			</div><!--/div #subheader-->
		</header><!--/header-->
		<section id="features">
			<div class="wrapper cf">
				<div class="features-box">

						<?php

							if ( get_theme_mod( 'sheldon_frontpage_firstlybox_title','Entwicklung erleben' ) ) {

								echo '<h4>';

									echo get_theme_mod( 'sheldon_frontpage_firstlybox_title','Entwicklung erleben' );

								echo '</h4>';
							}

							if ( get_theme_mod( 'sheldon_frontpage_firstlybox_content','PEKiP ist eine Begleitung der frühkindlichen Entwicklung durch Bewegung, Sinnes- und Spielanregung, dadurch kann die Entwicklung und momentane Situation des Säuglings erkannt, begleitet und gefördert werden.' ) ) {

								echo '<p>';

									echo get_theme_mod( 'sheldon_frontpage_firstlybox_content','PEKiP ist eine Begleitung der frühkindlichen Entwicklung durch Bewegung, Sinnes- und Spielanregung, dadurch kann die Entwicklung und momentane Situation des Säuglings erkannt, begleitet und gefördert werden.' );

								echo '</p>';
							}
						?>
				</div><!--/div .features-box-->
				<div class="features-box">

						<?php

							if ( get_theme_mod( 'sheldon_frontpage_secondlybox_title','Gemeinsam spielen & entdecken' ) ) {

								echo '<h4>';

									echo get_theme_mod( 'sheldon_frontpage_secondlybox_title','Gemeinsam spielen & entdecken' );

								echo '</h4>';
							}

							if ( get_theme_mod( 'sheldon_frontpage_secondlybox_content', 'Ziel des PEKiP ist, Eltern und Babies in dem sensiblen Prozess des Zueinanderfindens zu begleiten und zu unterstützen, sowie den Kontakt und Erfahrungsaustausch unter Eltern zu ermöglichen' ) ) {

								echo '<p>';

									echo get_theme_mod( 'sheldon_frontpage_secondlybox_content', 'Ziel des PEKiP ist, Eltern und Babies in dem sensiblen Prozess des Zueinanderfindens zu begleiten und zu unterstützen, sowie den Kontakt und Erfahrungsaustausch unter Eltern zu ermöglichen' );

								echo '</p>';
							}
						?>

				</div><!--/div .features-box-->
				<div class="features-box">

						<?php

							if ( get_theme_mod( 'sheldon_frontpage_thirdlybox_title','Gesunde & ausgewogene Ernährung' ) ) {

								echo '<h4>';

									echo get_theme_mod( 'sheldon_frontpage_thirdlybox_title','Gesunde & ausgewogene Ernährung' );

								echo '</h4>';
							}

							if ( get_theme_mod( 'sheldon_frontpage_thirdlybox_content','Mit einer altersgerechten Ernährung, gerade im 1. Lebensjahr, legen sie den Grundstein für die lebenslange Gesundheit ihrees Kindes. In meinen Vorträgen können sie alle wichtigen Informationen und individuelle Tipps bekommen.' ) ) {

								echo '<p>';

									echo get_theme_mod( 'sheldon_frontpage_thirdlybox_content','Mit einer altersgerechten Ernährung, gerade im 1. Lebensjahr, legen sie den Grundstein für die lebenslange Gesundheit ihrees Kindes. In meinen Vorträgen können sie alle wichtigen Informationen und individuelle Tipps bekommen.' );

								echo '</p>';
							}
						?>

				</div><!--/div .features-box-->
			</div><!--/div .wrapper-->
		</section><!--/section #features-->
		<section id="content">
			<div class="wrapper">
				<div class="content-article cf" role="main">
		     <?php
		      $args = array( 'posts_per_page' =>  get_theme_mod( 'sheldon_frontpage_posts','3' ) );
		      $myposts = get_posts( $args );
		      foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		       <div class="frontpage-post">
				    <div class="frontpage-post-thumbnail">
						<?php
				     if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					    the_post_thumbnail( array(100, 100) );
             }
            ?>
            </div>
				   <h3><a class="frontpage-post-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				   <div class="frontpage-post-date"><?php the_time('d.m.Y'); ?></div>
				   <p><?php the_excerpt();?><a href="<?php the_permalink(); ?>">Weiterlesen</a></p>
			    </div>
		      <?php endforeach;
		      wp_reset_postdata();?>
				</div><!--/div .content-article .cf-->
			</div><!--/div .wrapper-->
		</section><!--/section #content-->

		<?php } get_footer(); ?>
