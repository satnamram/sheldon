# sheldon
a wordpress theme based on lawyeria-lite.1.0.7 customized for personal needs.
All credit goes to to the devs of the original theme.

original repository: https://github.com/Codeinwp/lawyeria-lite

# License

Unless otherwise specified, all the theme files, scripts and images are licensed under GNU General Public License version 3.0.

The exceptions to this license are as follows: 
* fancybox: www.fancyapps.com/fancybox/#license
* jQuery Masonry v2.0.110517 beta: MIT license
* Lato font : http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
* Roboto Slab font: http://www.apache.org/licenses/LICENSE-2.0.html
