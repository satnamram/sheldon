<?php
/**
 *  The template for displaying Sidebar.
 *
 *  @package sheldon
 */
?>
<aside id="sidebar-right">
	<?php dynamic_sidebar( 'right-sidebar' ); ?>
</aside><!--/aside #sidebar-right-->
