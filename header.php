<?php
/**
 *  The template for displaying Header.
 *
 *  @package sheldon
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta http-equiv="<?php echo get_template_directory_uri();?>/content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta charset="UTF-8">
		<title><?php wp_title('|', true, 'right'); ?></title>

		<?php wp_head(); ?>
		<?php
		// customizr overwrite
				echo '<style>';
				echo '.menu-item a{';
				echo '	background-color: '.get_theme_mod( 'sheldon_header_menu_color','#FFFFFF').';';
				echo '  border-radius: 10px;';
				echo '}';
				echo '.wide-nav{';
				echo '	background-color: '.get_theme_mod( 'sheldon_page_h_bgcolor','#ec9746').';';
				echo '	color: '.get_theme_mod( 'sheldon_page_h_fgcolor','#dc2b19').';';
				echo '}';
				echo '</style>';
		?>
	</head>
	<body <?php body_class(); ?>>
 	<header>
			<div class="wide-header">
				<div class="wrapper cf">
					<br/><br/><br/><br/><br/>
				</div><!--/div .wrapper-->
			</div><!--/div .wide-header-->

			<div class="wrapper cf">
			    <nav>
    				<div class="openresponsivemenu" style='background-color:<?php echo get_theme_mod( 'sheldon_header_mobile_bgcolor','#9fda40' ); ?>;color:<?php echo get_theme_mod( 'sheldon_header_mobile_fgcolor','#111111' ); ?>;'>
    					<?php _e('Menü anzeigen','sheldon'); ?>
    				</div><!--/div .openresponsivemenu-->
    				<div class="container-menu cf">
        				<?php
        					wp_nav_menu(
        					    array(
        						        'theme_location' => 'header-menu',
        							)
        						);
        					?>
    				</div><!--/div .container-menu .cf-->
    			</nav><!--/nav .navigation-->
		    </div>
				<div id="cornercircle-wrapper">
					<div id="cornercircle">
					</div>
					<?php
					if ( get_theme_mod( 'sheldon_header_logo', get_template_directory_uri() .'/images/header-logo.png' ) ) {
							echo '<img id="logo" src="'. get_theme_mod( 'sheldon_header_logo', get_template_directory_uri() .'/images/header-logo.png' ) .'" alt="'. get_bloginfo( 'name' ) .'" title="'. get_bloginfo( 'name' ) .'" />';
					}
					?>
					<div id="cornertext">
						Rita Schumann
					</div>
			 </div>
