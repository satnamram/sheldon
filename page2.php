		<?php
		/**
		 *  The template for displaying Page.
		 *
		 *  @package sheldon
         *
		 *	Template Name: Standardtemplate 2
		 */
		get_header();
		?>

		<?php
		// customizr overwrite for page 2
				echo '<style>';
				echo '.wide-nav{';
				echo '	background-color: '.get_theme_mod( 'sheldon_page2_h_bgcolor','#c3d8a2').';';
				echo '	color: '.get_theme_mod( 'sheldon_page2_h_fgcolor','#dc2b19').';';
				echo '}';
				echo '</style>';
		?>

  			<section class="wide-nav">
				<div class="wrapper">
					<h3>
						<?php the_title(); ?>
					</h3><!--/h3-->
				</div><!--/div .wrapper-->
			</section><!--/section .wide-nav-->
		</header><!--/header-->
		<section id="content">
			<div class="wrapper cf">
				<div id="posts">
					<?php
						if ( have_posts() ) : while ( have_posts() ) : the_post();

					?>
					<div class="post">

						<div class="post-excerpt">
							<?php the_content(); ?>
						</div><!--/div .post-excerpt-->


					</div><!--/div .post-->
					<?php endwhile; else: ?>
                    	<p><?php _e('Sorry, no posts matched your criteria.', 'sheldon'); ?></p>
                	<?php endif; ?>
				</div><!--/div #posts-->
				<?php get_sidebar(); ?>
			</div><!--/div .wrapper-->
		</section><!--/section #content-->
		<?php get_footer(); ?>
