		<?php
		/**
		 *  The template for displaying Page.
		 *
		 *  @package sheldon
		 *
		 *	Template Name: Keine Sidebar
		 */
		get_header();
		?>

  			<section class="wide-nav">
				<div class="wrapper">
					<h3>
						<?php the_title(); ?>
					</h3><!--/h3-->
				</div><!--/div .wrapper-->
			</section><!--/section .wide-nav-->
		</header><!--/header-->
		<section id="content">
			<div class="wrapper cf">
				<div id="posts" style="width:100%;">
					<?php
						if ( have_posts() ) : while ( have_posts() ) : the_post();

					?>
					<div class="post">

						<div class="post-excerpt">
							<?php the_content(); ?>
						</div><!--/div .post-excerpt-->


					</div><!--/div .post-->
					<?php endwhile; else: ?>
                    	<p><?php _e('Sorry, no posts matched your criteria.', 'sheldon'); ?></p>
                	<?php endif; ?>
				</div><!--/div #posts-->
			</div><!--/div .wrapper-->
		</section><!--/section #content-->
		<?php get_footer(); ?>
